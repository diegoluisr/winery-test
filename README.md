# Winery (by Diego Restrepo)

This is a Lumen implementation to show my skills in PHP and Lumen/Laravel framework.

## Development Environment

This project uses Docker and Lando as warranty to set an homogeneous development environment. Please, find the proper instructions in the links bellow to complete the installation.

*  [Docker for mac](https://docs.docker.com/docker-for-mac/) 
*  [Docker for Windows](https://docs.docker.com/docker-for-windows/) 
*  [Lando](https://docs.lando.dev/basics/installation.html)


## Requirements
To check the project running you must to fullfil the next steps

### Environment file (.env)

Find and copy the file located at *'/api/.env.example'* and name the copy as *'.env'* this way you could have control above some configurations, you could copy the next text also, in a new *'.env'* file.

```
APP_NAME="Project Name (Winery)"
APP_ENV=local
APP_KEY=gntaFaU6ZxfEtMJHyyoRGyfdOpQKU1o4
APP_DEBUG=false
APP_URL=http://localhost
APP_TIMEZONE=UTC

DB_CONNECTION=mysql
DB_HOST=database
DB_PORT=3306
DB_DATABASE=lamp
DB_USERNAME=lamp
DB_PASSWORD=lamp

ELASTICEMAIL_API_URL=https://api.elasticemail.com/v2/
ELASTICEMAIL_API_KEY= aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa

EMAIL_DEFAULT_FROM=support@winery.com
EMAIL_SMTP_USER=support@winery.com
EMAIL_SMTP_PASSWORD=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
EMAIL_SMTP_SERVER=smtp.elasticemail.com
EMAIL_SMTP_PORT=2525
EMAIL_DEFAULT_FROM_NAME="Winery"
EMAIL_DEFAULT_REPLY_TO=no-replay@winery.com
EMAIL_DEFAULT_REPLY_TO_NAME="Winery - No Reply"
EMAIL_DEFAULT_CHARSET=utf8

CACHE_DRIVER=file
QUEUE_CONNECTION=database

JWT_SECRET=SECRETKEY
```

### Database import
You have several ways to import the database to the project

1.  Using Artisan: use the command ```> lando php artisan migrate:fresh --seed ``` inside the *api* folder, this command migrates the database schema directy from the migration files in the project located at */api/database/migrations* and the seeder located at */api/database/seeds*
2.  Importing database from backup wit lando. A compressed file locate at */bd* folder could be used to import the database using this command ```> lando db-import <file> ```
3.  Importing database form backup with MySQLWorkbenck/PHPMyAdmin. A compressed file locate at */bd* folder could be used to import the database in the same status as migrated with artisan. you could use MySQlWorkbenck


### Init Lando

When all the tools are ready, use your *terminal* to open the project location. The use the bellow command, it uses Lando to start services/containers in Docker that allows you to run the application locally.

```
> lando start
```

### Virtual Host (if you are not using docker lando)

If you have issues to install Docker/Lando, you could still use a XAMP or LAMP distribution. I that case you must to config a VHOST to aim tho the bellow folder, this way we sure that sensible information will be out of view to visitors.

```
/api/public/
```

## Test

You could use the next steps to test different behaviors over the application

### Services / API

To check manually the endpoints created for this code test, you could import the bellow files in Postman

*  /postman/API.postman_collection.json
*  /postman/Local-Lando-Docker.postman_environment.json

### Code Quality

Use the command below to test the code quality.

```
> lando php api/vendor/squizlabs/php_codesniffer/bin/phpcs ./api/app/
```

### Email notification

I have created a scheduled behavior that send wines in its peak as best drinkable, to test it manually use the command below

```
> lando php artisan notification:wines 10 1
```

The custom command send notifications groups based on User ID, and based on the MOD math function MOD(Dividend, Divisor), this way the system load is reduced. Each email job is added to a Queue, this job list could be checked directly using mysql. use the bellow lando command to access database.

```
> lando mysql
```

# What to check?

You could find some implemented behaviors.

## Authentication

Check the JWT middleware (api/app/Http/Middleware/JwtAuthenticate.php) This uses JWT (JSON Web Tocken) to allow a simple and secure communication between the client and server. This commponent is abled in bootstrap file (app/bootstrap/app.php) in line 65

## Authorization

Check the Auth Service provider (api/app/Http/Providers/AuthServiceProvider.php) This uses a custom implementation that uses Laravel policies to allow or disallow user access to some parts of the app (api/app/Providers/Policies/WinePolicy.php) This commponent is abled in bootstrap file (app/bootstrap/app.php) in line 85

## Work queue

TO check current items in the work queue you could use 'MySQL' accesing with the command above and check the table 'jobs'
```
> lando mysql
```

```
mysql> USE lamp;
mysql> SELECT * FROM jobs;
```

If you want to check failed jobs you could check 'failed_jobs' table
```
> lando mysql
```

```
mysql> USE lamp;
mysql> SELECT * FROM failed_jobs;
```

### Email Job

In this code reposotory you could see a custom Job that uses 'Elastic Email' as email provider. You must replace Elastic email and Email variables in the '.env' file with your current credentials.

Check (api/app/Http/Providers/ElasticEmailServiceProvider.php) it is configured in bootstrap file (app/bootstrap/app.php) in line 86.

If you find this document is not enoght to be installed or you have a suggestion, please email me [diegoluisr@gmail.com](mailto:diegoluisr@gmail.com) 


