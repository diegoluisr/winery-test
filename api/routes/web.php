<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json(['version' => $router->app->version()]);
});

// Public endpoints
$router->group(
    ['middleware' => []],
    function() use ($router) {

        // Classifications
        $router->get('classifications', 'ClassificationController@getList');
        $router->get('classification/{classification_id:[0-9]+}', 'ClassificationController@getOne');
        
        // Grape Types
        $router->get('grape-types', 'GrapeTypeController@getList');
        $router->get('grape-type/{grape_type_id:[0-9]+}', 'GrapeTypeController@getOne');

        // Media
        $router->get('media/{media_id:[0-9]+}', 'MediaController@getMediaAction');
        $router->get('media/{id:[0-9]+}/{entity_type:[aA-zZ]+}', 'MediaController@index');

        // Media Group
        $router->get('media-group', 'MediaGroupController@listAll');
        $router->get('media-group/{mediagroup_id:[0-9]+}', 'MediaGroupController@show');

        // Media Type
        $router->get('media-type', 'MediaTypeController@listAll');
        $router->get('media-type/{media_id:[0-9]+}', 'MediaTypeController@show');

        // Taste Notes
        $router->get('taste-notes', 'TasteNoteController@getList');
        $router->get('taste-note/{taste_note_id:[0-9]+}', 'TasteNoteController@getOne');

        // Units
        $router->get('units', 'UnitController@getList');
        $router->get('unit/{unit_id:[0-9]+}', 'UnitController@getOne');

        // User
        $router->post('login', 'AuthController@emailLogin');
        $router->post('register', 'AuthController@emailRegister');
        $router->post('recover', 'AuthController@recoverPassword');

        // Wine Types
        $router->get('wine-types', 'WineTypeController@getList');
        $router->get('wine-type/{wine_type_id:[0-9]+}', 'WineTypeController@getOne');

        // Wineries
        $router->get('wineries', 'WineryController@getList');
        $router->get('winery/{winery_id:[0-9]+}', 'WineryController@getOne');

        // Wines
        $router->get('wines', 'WineController@getList');
        $router->get('wines/timeline', 'WineController@timeline');
        $router->get('wine/{wine_id:[0-9]+}', 'WineController@getOne');
    }
);

// Private endpoints
$router->group(
    ['middleware' => ['jwt.auth']],
    function() use ($router) {

        // Classifications
        $router->post('classification', 'ClassificationController@create');
        $router->put('classification/{classification_id:[0-9]+}', 'ClassificationController@update');
        $router->delete('classification/{classification_id:[0-9]+}', 'ClassificationController@delete');
        
        // Grape Types
        $router->post('grape-type', 'GrapeTypeController@create');
        $router->put('grape-type/{grape_type_id:[0-9]+}', 'GrapeTypeController@update');
        $router->delete('grape-type/{grape_type_id:[0-9]+}', 'GrapeTypeController@delete');

        // Media
        $router->post('media', 'MediaController@uploadAction');
        $router->get('media/private/{media_id:[0-9]+}/{fname}', 'MediaController@proxyFile');

        // Media Type
        $router->post('media-type', 'MediaTypeController@create');
        $router->put('media-type/{media_id:[0-9]+}', 'MediaTypeController@put');
        $router->delete('media-type/{media_id:[0-9]+}', 'MediaTypeController@delete');

        // Media Group
        $router->post('media-group', 'MediaGroupController@create');
        $router->put('media-group/{mediagroup_id:[0-9]+}', 'MediaGroupController@put');
        $router->delete('media-group/{mediagroup_id:[0-9]+}', 'MediaGroupController@delete');

        // Taste Notes
        $router->post('taste-note', 'TasteNoteController@create');
        $router->put('taste-note/{taste_note_id:[0-9]+}', 'TasteNoteController@update');
        $router->delete('taste-note/{taste_note_id:[0-9]+}', 'TasteNoteController@delete');

        // Units
        $router->post('unit', 'UnitController@create');
        $router->put('unit/{unit_id:[0-9]+}', 'UnitController@update');
        $router->delete('unit/{unit_id:[0-9]+}', 'UnitController@delete');

        // Wine Types
        $router->post('wine-type', 'WineTypeController@create');
        $router->put('wine-type/{wine_type_id:[0-9]+}', 'WineTypeController@update');
        $router->delete('wine-type/{wine_type_id:[0-9]+}', 'WineTypeController@delete');

        // Wineries
        $router->post('winery', 'WineryController@create');
        $router->put('winery/{winery_id:[0-9]+}', 'WineryController@update');
        $router->delete('winery/{winery_id:[0-9]+}', 'WineryController@delete');

        // Wines
        $router->post('wine', 'WineController@create');
        $router->put('wine/{wine_id:[0-9]+}', 'WineController@update');
        $router->delete('wine/{wine_id:[0-9]+}', 'WineController@delete');
        $router->patch('wine/open-bottle/{wine_id:[0-9]+}', 'WineController@removeOpenedBottle');
    }
);