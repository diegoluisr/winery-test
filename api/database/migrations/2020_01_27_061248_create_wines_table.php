<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('unit_id');
            $table->unsignedBigInteger('winery_id');
            $table->unsignedInteger('grape_type_id');
            $table->unsignedInteger('wine_type_id');
            $table->unsignedInteger('classification_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->integer('quantity');
            $table->string('name', 45);
            $table->timestamp('year', 0);
            $table->unsignedInteger('age_until'); // in years
            $table->string('description', 45);
            $table->unsignedDecimal('value');
        });

        Schema::table('wines', function (Blueprint $table) {
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('winery_id')->references('id')->on('wineries');
            $table->foreign('grape_type_id')->references('id')->on('grape_types');
            $table->foreign('wine_type_id')->references('id')->on('wine_types');
            $table->foreign('classification_id')->references('id')->on('classifications');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wines');
    }
}
