<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('entity_type', 255);
            $table->unsignedInteger('quantity');
            $table->boolean('is_private');
            $table->boolean('is_visible');
            $table->unsignedInteger('media_type_id');
        });

        Schema::table('media_groups', function (Blueprint $table) {
            $table->foreign('media_type_id')->references('id')->on('media_types'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_groups');
    }
}
