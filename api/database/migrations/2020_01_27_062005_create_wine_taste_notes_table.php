<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWineTasteNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_taste_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wine_id');
            $table->unsignedInteger('taste_note_id');
        });

        Schema::table('wine_taste_notes', function (Blueprint $table) {
            $table->foreign('wine_id')->references('id')->on('wines'); 
            $table->foreign('taste_note_id')->references('id')->on('taste_notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_taste_notes');
    }
}
