<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('is_private')->default(FALSE);
            $table->string('filename', 600);
            $table->string('filepath', 600);
            $table->json('metadata');
            $table->morphs('mediable');
            $table->unsignedInteger('media_group_id');
            $table->boolean('is_checked')->default(FALSE);
        });

        Schema::table('media', function (Blueprint $table) {
            $table->foreign('media_group_id')->references('id')->on('media_groups'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
