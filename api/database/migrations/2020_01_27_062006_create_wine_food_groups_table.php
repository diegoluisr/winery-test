<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWineFoodGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_food_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wine_id');
            $table->unsignedInteger('food_group_id');
        });

        Schema::table('wine_food_groups', function (Blueprint $table) {
            $table->foreign('wine_id')->references('id')->on('wines'); 
            $table->foreign('food_group_id')->references('id')->on('food_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_food_groups');
    }
}
