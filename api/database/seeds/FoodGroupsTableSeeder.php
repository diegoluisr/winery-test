<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class FoodGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('food_groups')->insert([
            ['id' => 1, 'name' => 'Fish'],
            ['id' => 2, 'name' => 'Meat'],
            ['id' => 3, 'name' => 'Chicken'],
        ]);
    }
}
