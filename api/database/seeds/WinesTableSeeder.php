<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wines')->insert([
            ['id' => 1, 'unit_id' => 1, 'user_id' => 1, 'winery_id' => 1, 'grape_type_id' => 1, 'wine_type_id' => 1, 'quantity' => 1000, 'name' => 'Test Wine 1', 'year' => Carbon::now()->subMonths(50)->floorMonth(), 'age_until' => 2, 'description' => 'Description test 1', 'value' => 59.99, 'classification_id' => 1],
            ['id' => 2, 'unit_id' => 1, 'user_id' => 2, 'winery_id' => 2, 'grape_type_id' => 2, 'wine_type_id' => 2, 'quantity' => 500, 'name' => 'Test Wine 2', 'year' => Carbon::now()->subMonths(20)->floorMonth(), 'age_until' => 6, 'description' => 'Description test 2', 'value' => 39.99, 'classification_id' => 2],
            ['id' => 3, 'unit_id' => 1, 'user_id' => 3, 'winery_id' => 3, 'grape_type_id' => 1, 'wine_type_id' => 3, 'quantity' => 200, 'name' => 'Test Wine 3', 'year' => Carbon::now()->subMonths(10)->floorMonth(), 'age_until' => 3, 'description' => 'Description test 3', 'value' => 49.99, 'classification_id' => null],
            ['id' => 4, 'unit_id' => 1, 'user_id' => 4, 'winery_id' => 1, 'grape_type_id' => 1, 'wine_type_id' => 1, 'quantity' => 1000, 'name' => 'Test Wine 4 - Notification', 'year' => Carbon::now()->subMonths(24)->floorMonth(), 'age_until' => 2, 'description' => 'Description test 1', 'value' => 59.99, 'classification_id' => 1],
            ['id' => 5, 'unit_id' => 1, 'user_id' => 1, 'winery_id' => 2, 'grape_type_id' => 2, 'wine_type_id' => 2, 'quantity' => 500, 'name' => 'Test Wine 5 - Notification', 'year' => Carbon::now()->subMonths(24)->floorMonth(), 'age_until' => 2, 'description' => 'Description test 2', 'value' => 39.99, 'classification_id' => 2],
            ['id' => 6, 'unit_id' => 1, 'user_id' => 1, 'winery_id' => 3, 'grape_type_id' => 1, 'wine_type_id' => 3, 'quantity' => 200, 'name' => 'Test Wine 6 - Notification', 'year' => Carbon::now()->subMonths(24)->floorMonth(), 'age_until' => 2, 'description' => 'Description test 3', 'value' => 49.99, 'classification_id' => null],
        ]);

        DB::table('wine_taste_notes')->insert([
            ['id' => 1, 'wine_id' => 1, 'taste_note_id' => 1],
            ['id' => 2, 'wine_id' => 1, 'taste_note_id' => 2],
            ['id' => 3, 'wine_id' => 1, 'taste_note_id' => 3],
            ['id' => 4, 'wine_id' => 1, 'taste_note_id' => 4],
            ['id' => 5, 'wine_id' => 2, 'taste_note_id' => 2],
            ['id' => 6, 'wine_id' => 2, 'taste_note_id' => 3],
            ['id' => 7, 'wine_id' => 2, 'taste_note_id' => 4],
            ['id' => 8, 'wine_id' => 2, 'taste_note_id' => 5],
            ['id' => 9, 'wine_id' => 3, 'taste_note_id' => 3],
            ['id' => 10, 'wine_id' => 3, 'taste_note_id' => 4],
            ['id' => 11, 'wine_id' => 3, 'taste_note_id' => 5],
            ['id' => 12, 'wine_id' => 3, 'taste_note_id' => 6],
        ]);

        DB::table('wine_food_groups')->insert([
            ['id' => 1, 'wine_id' => 1, 'food_group_id' => 1],
            ['id' => 2, 'wine_id' => 1, 'food_group_id' => 2],
            ['id' => 5, 'wine_id' => 2, 'food_group_id' => 2],
            ['id' => 6, 'wine_id' => 2, 'food_group_id' => 3],
            ['id' => 9, 'wine_id' => 3, 'food_group_id' => 3],
            ['id' => 10, 'wine_id' => 3, 'food_group_id' => 1],
        ]);
    }
}
