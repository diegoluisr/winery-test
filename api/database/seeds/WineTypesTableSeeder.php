<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class WineTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wine_types')->insert([
            ['id' => 1, 'name' => 'Red'],
            ['id' => 2, 'name' => 'White'],
            ['id' => 3, 'name' => 'Sparkling'],
        ]);
    }
}
