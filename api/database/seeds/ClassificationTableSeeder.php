<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classifications')->insert([
            ['id' => 1, 'name' => 'Grand Cru Classe'],
            ['id' => 2, 'name' => 'Grand Cru'],
            ['id' => 3, 'name' => 'Premier Cru'],
        ]);
    }
}
