<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class WineriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wineries')->insert([
            ['id' => 1, 'name' => 'Château de Goulaine', 'founding_year' => '1000', 'region' => 'Fance / Loire'],
            ['id' => 2, 'name' => 'Barone Ricasoli', 'founding_year' => '1141', 'region' => 'Italy / Tuscany'],
            ['id' => 3, 'name' => 'Schloss Johanisberg', 'founding_year' => '1100', 'region' => 'Germany / Riesling'],
            ['id' => 4, 'name' => 'Codorniu', 'founding_year' => '1551', 'region' => 'Spain / Riesling'],
            ['id' => 5, 'name' => 'Casa Madero, Parras de la Fuente', 'founding_year' => '1597', 'region' => 'Mexico / Coahuila'],
        ]);
    }
}
