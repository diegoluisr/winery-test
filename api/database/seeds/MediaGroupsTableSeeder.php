<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediaGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_groups')->insert([
            ['id' => 1, 'name' => 'winery.cover', 'entity_type' => 'App\Models\Winery', 'quantity' => 1, 'is_private' => false, 'is_visible' => true, 'media_type_id' => 1],
            ['id' => 2, 'name' => 'wine.cover', 'entity_type' => 'App\Models\Wine', 'quantity' => 1, 'is_private' => false, 'is_visible' => true, 'media_type_id' => 1],
        ]);
    }
}