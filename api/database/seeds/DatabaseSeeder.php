<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('MediaTypesTableSeeder');
        $this->call('MediaGroupsTableSeeder');
        $this->call('MediaTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('UnitsTableSeeder');
        $this->call('ClassificationsTableSeeder');
        $this->call('WineTypesTableSeeder');
        $this->call('GrapeTypesTableSeeder');
        $this->call('TasteNotesTableSeeder');
        $this->call('FoodGroupsTableSeeder');
        $this->call('WineriesTableSeeder');
        $this->call('WinesTableSeeder');
    }
}
