<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            ['id' => 1, 'name' => '750 ml bottle'],
            ['id' => 2, 'name' => 'Magnum bottle'],
            ['id' => 3, 'name' => 'Small crate consisting of 6x 750 ml bottle'],
            ['id' => 4, 'name' => 'Large crate consisting of 12x 750 ml bottle'],
        ]);
    }
}
