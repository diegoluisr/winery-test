<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class GrapeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grape_types')->insert([
            ['id' => 1, 'name' => 'Red', 'preferred_terrain' => 'dry', 'preferred_climate' => 'sunny'],
            ['id' => 2, 'name' => 'White', 'preferred_terrain' => 'dry', 'preferred_climate' => 'sunny'],
        ]);
    }
}
