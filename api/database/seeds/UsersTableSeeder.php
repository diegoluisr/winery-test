<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => 1, 'role_id' => 1, 'email' => 'diegoluisr@gmail.com', 'password' => Crypt::encrypt('123456'), 'name' => 'Diego Restrepo'],
            ['id' => 2, 'role_id' => 2, 'email' => 'arjan@loyals.com ',  'password' => Crypt::encrypt('123456'), 'name' => 'Arjan van den Broek'],
            ['id' => 3, 'role_id' => 3, 'email' => 'alex@loyals.com',  'password' => Crypt::encrypt('123456'), 'name' => 'Alex Hollander'],
            ['id' => 4, 'role_id' => 4, 'email' => 'youri@loyals.com',  'password' => Crypt::encrypt('123456'), 'name' => 'Youri Lieberton'],
        ]);
    }
}
