<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class TasteNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taste_notes')->insert([
            ['id' => 1, 'name' => 'Melony'],
            ['id' => 2, 'name' => 'Rich'],
            ['id' => 3, 'name' => 'Expenssive'],
            ['id' => 4, 'name' => 'Limpid'],
            ['id' => 5, 'name' => 'Floral'],
            ['id' => 6, 'name' => 'Gold'],
            ['id' => 7, 'name' => 'Buttery'],
            ['id' => 8, 'name' => 'Honeyed'],
            ['id' => 9, 'name' => 'Peach/Appricot'],
            ['id' => 10, 'name' => 'Botrytis'],
            ['id' => 11, 'name' => 'Heady'],
            ['id' => 12, 'name' => 'Lanolin'],
            ['id' => 13, 'name' => 'Honeysuckle/Hazlenut'],
            ['id' => 14, 'name' => 'Barley sugar'],
            ['id' => 15, 'name' => 'New wood'],
        ]);
    }
}
