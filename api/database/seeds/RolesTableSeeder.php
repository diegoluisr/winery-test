<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => 1, 'name' => 'sysadmin'],
            ['id' => 2, 'name' => 'admin'],
            ['id' => 3, 'name' => 'editor'],
            ['id' => 4, 'name' => 'customer']
        ]);
    }
}