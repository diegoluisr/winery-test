<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media')->insert([
            ['id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'is_private' => 0, 'filename' => '/default.png', 'filepath' => '', 'metadata' => '[]', 'mediable_type' => 'App\Models\Winery', 'mediable_id' => 1, 'media_group_id' => 1, 'is_checked' => 0],
            ['id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'is_private' => 0, 'filename' => '/default.png', 'filepath' => '', 'metadata' => '[]', 'mediable_type' => 'App\Models\Wine', 'mediable_id' => 1, 'media_group_id' => 2, 'is_checked' => 0],
        ]);
    }
}