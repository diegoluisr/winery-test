<?php
/**
 * This document is open source
 * file: api/app/Models/MediaGroup.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Models
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class MediaGroup extends Model
{

    public const USER_PICTURE = 'user_picture';
    public const PROPERTY_IMAGE = 'property_image';
    public const CONTRACT_FILE_ID = 9;
    public const USER_SIGNATURE = 3;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media_groups';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['media_type_id'];

    protected $casts = [
        'is_private' => 'boolean',
        'is_visible' => 'boolean',
    ];

    /**
     * Funcion que retorna los datos relacionados al login alternativo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mediaType()
    {
        return $this->belongsTo('App\Models\MediaType');
    }

}
