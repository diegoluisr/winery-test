<?php
/**
 * This document is open source
 * file: api/app/Models/Media.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Models
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class Media extends Model
{

    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
  
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media';

    protected $dates = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mediable_type', 'mediable_id', 'media_group_id', 'deleted_at',
        'media_type_id', 'created_at', 'updated_at'
    ];

    protected $fillable = [];

    protected $casts = [
        'is_private' => 'boolean',
    ];

    /**
     * Funcion que retorna los datos relacionados al login alternativo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mediaGroup()
    {
        return $this->belongsTo('App\Models\MediaGroup');
    }

}
