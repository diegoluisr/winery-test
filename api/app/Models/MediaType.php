<?php
/**
 * This document is open source
 * file: api/app/Models/MediaType.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Models
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class MediaType extends Model
{

    public const IMAGE = 1;
    public const VIDEO = 2;
    public const AUDIO = 3;
    public const DOCUMENT = 4;
    public const ANY = 5;

    const CREATED_AT = null;
    const UPDATED_AT = null;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media_types';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
}
