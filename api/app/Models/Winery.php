<?php
/**
 * This document is open source
 * file: api/app/Models/Winery.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Models
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class Winery extends Model
{

    const CREATED_AT = null;
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wineries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'founding_year', 'region'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Funcion que retorna los datos relacionados al login alternativo
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function cover()
    {
        return $this->MorphOne('App\Models\Media', 'mediable')
            ->where('media_group_id', 1);
    }
}
