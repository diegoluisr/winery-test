<?php
/**
 * This document is open source
 * file: api/app/Models/Wine.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Models
 * @package  App\Models
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class Wine extends Model
{

    const CREATED_AT = null;
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'unit_id',
        'winery_id',
        'grape_type_id',
        'wine_type_id',
        'quantity',
        'name',
        'year',
        'age_until',
        'classification',
        'description',
        'value',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'unit_id',
        'winery_id',
        'grape_type_id',
        'wine_type_id',
    ];
    
    /**
     * The attributes appends to the model's JSON form.
     *
     * @var array
     */
    protected $appends = ['best_drinkable_after'];

    /**
     * Get the user's first name.
     *
     * @param string $value Attribute value
     * 
     * @return string
     */
    public function getYearAttribute($value)
    {
        return str_replace('-01 00:00:00', '', $value);
    }

    /**
     * Get the user's first name.
     *
     * @param string $value Attribute value
     * 
     * @return string
     */
    public function setYearAttribute($value)
    {
        $this->attributes['year'] = $value . '-01 00:00:00';
    }

    /**
     * Get the user's first name.
     *
     * @return string
     */
    public function getBestDrinkableAfterAttribute()
    {
        $best = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['year']);
        return $best->addYears($this->attributes['age_until'])
            ->format('Y-m');
    }

    /**
     * Funcion que retorna los datos relacionados al login alternativo
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function cover()
    {
        return $this->MorphOne('App\Models\Media', 'mediable')
            ->where('media_group_id', 2);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasteNotes()
    {
        return $this->belongsToMany(TasteNote::class, 'wine_taste_notes');
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function foodGroups()
    {
        return $this->belongsToMany(FoodGroup::class, 'wine_food_groups');
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classification()
    {
        return $this->belongsTo(Classification::class);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wineType()
    {
        return $this->belongsTo(WineType::class);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grapeType()
    {
        return $this->belongsTo(GrapeType::class);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * The roles that belong to the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function winery()
    {
        return $this->belongsTo(Winery::class);
    }

}
