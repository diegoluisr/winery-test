<?php
/**
 * This document is open source
 * file: api/app/Providers/Policies/WinePolicy.php
 * 
 * PHP version 7
 * 
 * @category Policies
 * @package  App\Providers\Policies
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Providers\Policies;

use App\Models\Role;
use App\Models\User;
use App\Models\Wine;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Policies
 * @package  App\Providers\Policies
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class WinePolicy
{
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user User object
     * @param \App\Models\Wine $wine Wine Object
     * 
     * @return bool
     */
    public function create(User $user, Wine $wine)
    {
        // Wines could be created for login users only
        if (!is_object($user)) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user User object
     * @param \App\Models\Wine $wine Wine Object
     * 
     * @return bool
     */
    public function update(User $user, Wine $wine)
    {
        // Allow if the current user is the owner of the wine 
        if ($user->id === $wine->user_id) {
            return true;
        }

        // Allow if the current user is sysadmin, admin or editor
        if (in_array($user->role->id, [Role::SYSADMIN, Role::ADMIN, Role::EDITOR])) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user User object
     * @param \App\Models\Wine $wine Wine Object
     * 
     * @return bool
     */
    public function delete(User $user, Wine $wine)
    {
        // Allow if the current user is the owner of the wine 
        if ($user->id === $wine->user_id) {
            return true;
        }

        // Allow if the current user is sysadmin, admin or editor
        if (in_array($user->role->id, [Role::SYSADMIN, Role::ADMIN, Role::EDITOR])) {
            return true;
        }

        return false;
    }

    
}