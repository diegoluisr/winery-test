<?php
/**
 * This document is open source
 * file: api/app/Providers/ElasticEmailServiceProvider.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Providers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Providers;

use ElasticEmailClient\ApiConfiguration;
use ElasticEmailClient\ElasticClient;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

/**
 * Clase para gestionar los trabajos en el envio de emails.
 * 
 * @category HttpController
 * @package  App\Providers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class ElasticEmailServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            ElasticClient::class, function ($app) {
                $configuration = new ApiConfiguration(
                    [
                    'apiUrl' => env('ELASTICEMAIL_API_URL'),
                    'apiKey' => env('ELASTICEMAIL_API_KEY')
                    ]
                );

                return new ElasticClient($configuration);
            }
        );
    }

    // /**
    //  * Bootstrap the application services.
    //  *
    //  * @return void
    //  */
    // public function boot() {
    //   //
    // }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ElasticClient::class];
    }

}
