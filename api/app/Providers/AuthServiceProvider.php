<?php
/**
 * This document is open source
 * file: api/app/Http/Providers/AuthServiceProvider.php
 * 
 * PHP version 7
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Providers;

use App\Models\User;
use App\Models\Wine;
use App\Providers\Policies\WinePolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Gate::policy(Wine::class, WinePolicy::class);
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest(
            'api', function ($request) {
                if ($request->input('api_token')) {
                    return User::where(
                        'api_token', $request->input('api_token')
                    )->first();
                }
            }
        );

        $this->register();

        Gate::define('create-wine', WinePolicy::class . '@create');
        Gate::define('update-wine', WinePolicy::class . '@update');
        Gate::define('delete-wine', WinePolicy::class . '@delete');
    }
}
