<?php
/**
 * This document is open source
 * file: api/app/Http/Middleware/Authenticate.php
 * 
 * PHP version 7
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
