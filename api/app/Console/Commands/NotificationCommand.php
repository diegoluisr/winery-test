<?php
/**
 * This document is open source
 * file: api/app/Console/Commands/NotificationCommand.php
 * 
 * PHP version 7
 * 
 * @category Commands
 * @package  App\Console\Commands
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Console\Commands;

use App\Jobs\ProcessEmailJob;
use App\Models\User;
use App\Models\Wine;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

/**
 * Class to send notification through email.
 * 
 * @category Commands
 * @package  App\Console\Commands
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class NotificationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "notification:wines {dividend} {divisor}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Notify users with the peack wines of the month";


    /**
     * Execute the notification console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $dividend = abs(intval($this->argument('dividend')));
            $divisor = abs(intval($this->argument('divisor')));

            $now = Carbon::now();
            $wines = Wine::query()
                ->whereRaw(
                    'YEAR(DATE_ADD(year, INTERVAL age_until YEAR)) = ? '
                        . 'AND MONTH(DATE_ADD(year, INTERVAL age_until YEAR)) = ?',
                    [$now->format('Y'), $now->format('m')]
                )->get();
                
            if (count($wines) > 0) {
                $wines = $wines->toArray();
                $users = User::whereRaw('MOD(id, ?) = ?', [$dividend, $divisor])
                    ->get();
                
                foreach ($users as $user) {
                    $this->_sendEmail($user->email, $wines);
                }

                $this->info('Wines: ' . count($wines));
            }
            
        } catch (Exception $e) {
            if (app()->environment('production')) {
                $this->error($e->getMessage());
            } else {
                $this->error($e->getTraceAsString());
            }
        }
    }

    /**
     * Private function to dispatch an email to be processed by system queue
     * 
     * @param string $email Target email
     * @param array  $wines Wine list with the recent peak wines
     * 
     * @return void
     */
    private function _sendEmail(string $email, array $wines)
    {
        dispatch(
            (new ProcessEmailJob(
                [
                    'subject' => 'Winery - Peak wines of the month' ,
                    'to' => [$email],
                    'template' => '106934',
                    'merge' => [
                        'message' => 'Hey, please check our site to see the peak wines of the month.',
                        'wines' => count($wines)
                    ],
                ]
            ))->onQueue('high')
        );
    }

}
