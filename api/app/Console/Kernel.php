<?php
/**
 * This document is open source
 * file: api/app/Console/Kernel.php
 * 
 * PHP version 7
 * 
 * @category Command
 * @package  App\Console
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Console;

use App\Console\Commands\NotificationCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Command
 * @package  App\Console
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        NotificationCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule Schedule object
     * 
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // We use the same command in different times to split the work load
        // each 15 minutes after 00:00:00 the first day of the month
        $schedule->command('notification:wines 10 0')->monthlyOn(1, '0:0');
        $schedule->command('notification:wines 10 1')->monthlyOn(1, '0:15');
        $schedule->command('notification:wines 10 2')->monthlyOn(1, '0:30');
        $schedule->command('notification:wines 10 3')->monthlyOn(1, '0:45');
        $schedule->command('notification:wines 10 4')->monthlyOn(1, '1:0');
        $schedule->command('notification:wines 10 5')->monthlyOn(1, '1:15');
        $schedule->command('notification:wines 10 6')->monthlyOn(1, '1:30');
        $schedule->command('notification:wines 10 7')->monthlyOn(1, '1:45');
        $schedule->command('notification:wines 10 8')->monthlyOn(1, '2:0');
        $schedule->command('notification:wines 10 9')->monthlyOn(1, '2:15');
    }
}
