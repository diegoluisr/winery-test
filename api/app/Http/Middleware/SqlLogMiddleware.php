<?php
/**
 * This document is open source
 * file: api/app/Http/Middleware/SqlLogMiddleware.php
 * 
 * PHP version 7
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class SqlLogMiddleware
{
    /**
     * Function manejadora del flujo
     * 
     * @param Request     $request Objeto con la solicitud
     * @param Closure     $next    Objeto con el Closure
     * @param string|null $guard   Guarda
     * 
     * @return Closure
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (app()->environment('local') || app()->environment('dev')) {
            DB::enableQueryLog();
        }

        $response = $next($request);

        if ((app()->environment('local') || app()->environment('dev'))
            && get_class($response) == JsonResponse::class
        ) {
            $data = $response->getData();
            if (is_array($data)) {
                $data[] = DB::getQueryLog();
                $response->setData($data);
            } else {
                $data->sql = DB::getQueryLog();
                $response->setData($data);
            }
        }
        return $response;

    }

}