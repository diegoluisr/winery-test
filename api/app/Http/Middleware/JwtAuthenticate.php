<?php
/**
 * This document is open source
 * file: api/app/Http/Middleware/JwtAuthenticate.php
 * 
 * PHP version 7
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Middleware
 * @package  App\Http\Middleware
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class JwtAuthenticate
{
    /**
     * Function manejadora del flujo
     * 
     * @param Request     $request Objeto con la solicitud
     * @param Closure     $next    Objeto con el Closure
     * @param string|null $guard   Guarda
     * 
     * @return Closure
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $token = $request->header('authorization');
      
        if (!$token) {
            // Unauthorized response if token not there
            return response()->json(
                [
                'error' => 'Token not provided.'
                ], 401
            );
        }
        try {
            $token = str_replace('Bearer ', '', $token);
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json(
                [
                'error' => 'Provided token is expired.'
                ], 400
            );
        } catch(Exception $e) {
            return response()->json(
                [
                'error' => 'An error while decoding token.'
                ], 400
            );
        }
        $user = User::with(['role'])->find($credentials->sub);
        Auth::setUser($user);
        return $next($request);
    }

}
