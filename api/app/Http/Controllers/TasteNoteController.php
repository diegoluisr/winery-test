<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/TasteNoteController.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TasteNote;
use Illuminate\Http\Request;

/**
 * Clase para el manejo en la subida de archivos a la plataforma
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class TasteNoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $taste_note = new TasteNote($payload);

        $taste_note->save();

        return response()->json($taste_note);

    }


    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {
        $taste_notes = TasteNote::get();

        return response()->json($taste_notes);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $taste_note_id TasteNote ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $taste_note_id)
    {
        $taste_note = TasteNote::find($taste_note_id);

        if (!is_object($taste_note)) {
            abort(404, 'The taste note doesn\'t exists');
        }

        return response()->json($taste_note);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $taste_note_id TasteNote ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $taste_note_id)
    {

        $taste_note = TasteNote::find($taste_note_id);

        if (!is_object($taste_note)) {
            abort(404, 'The taste note doesn\'t exists');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $taste_note->update($payload);

        return response()->json($taste_note);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $taste_note_id TasteNote ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $taste_note_id)
    {
        $taste_note = TasteNote::find($taste_note_id);

        if (!is_object($taste_note)) {
            abort(404, 'The taste note doesn\'t exists');
        }

        $taste_note->delete();

        return response()->json($taste_note);
    }

}
