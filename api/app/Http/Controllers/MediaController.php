<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/MediaController.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\MediaGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;


/**
 * Clase para el manejo en la subida de archivos a la plataforma
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class MediaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion para la subida de archivos tipo media.
     * 
     * @param Request $request Http request object
     * 
     * @return Json data
     */
    public function uploadAction(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'media' => 'bail|required| '
                    . 'mimetypes:"application/*","image/*","video/*","audio/*"',
                'group' => 'bail|required|string|exists:media_groups,name',
                'parent_id' => 'bail|required|integer',
                'is_private' => 'bail|sometimes|boolean',
            ]
        );

        return $this->upload($request, $payload['parent_id'], $payload['group']);
    }

    /**
     * Funcion para la subida de archivos tipo media.
     * 
     * @param Request $request Http request object
     * @param int     $id      Entity id.
     * @param string  $group   Media group name.
     *  
     * @return Json data
     */
    public function upload(Request $request, int $id, string $group)
    {
        $payload = $this->validate(
            $request, [
                'media' => 'bail|required| '
                    . 'mimetypes:"application/*","image/*","video/*","audio/*"',
            ]
        );

        $file = $request->file('media');

        $media_group = MediaGroup::select(['media_groups.*'])
            ->leftJoin(
                'media_types', 'media_groups.media_type_id', '=', 'media_types.id'
            )
            ->with(['mediaType'])
            ->where(
                [
                    [
                        'media_types.mime_types',
                        'LIKE',
                        '%' . $file->getMimeType() . '%'
                    ],
                    [
                        'media_groups.name',
                        $group
                    ]
                ]
            )
            ->first();


        if (is_null($media_group)) {
            abort('404', 'El media group no existe');
        }

        $media_group = $media_group->toArray();

        if ($media_group['quantity'] == 1) {
            $media = Media::where(
                [
                    ['media_group_id', $media_group['id']],
                    ['mediable_id', $id],
                    ['mediable_type', $media_group['entity_type']],
                ]
            )->first();

            if (!is_null($media)) {
                $media->delete();
            }
        } else {
            $count = Media::where(
                [
                    ['media_group_id', $media_group['id']],
                    ['mediable_id', $id],
                    ['mediable_type', $media_group['entity_type']],
                ]
            )->count();

            if ($count >= $media_group['quantity']) {
                abort('422', 'Too many files for the same group/entity');
            }
        }
        
        $folder = Str::slug(
            str_replace(
                'App\\Models\\', '', $media_group['entity_type']
            )
        );
        
        $path = 'uploads/' . $media_group['media_type']['name']
            . '/' . $folder . '/' . $id;
        
        // https://gist.github.com/deividaspetraitis/4cca4fa6a61cc9a75e12f640041e53f5
        // https://yantb.com/programacion/lumen/trabajar-archivos-en-lumen/
        // https://laravel.com/docs/5.8/filesystem#the-public-disk

        if (boolval($media_group['is_private'])) {
            $storage = Storage::disk('private');
        } else {
            $storage = Storage::disk('public');
        }
        $stored = $storage->put($path, $file);

        $media = new Media;
        $media->filename = str_replace($path, '', $stored);
        $media->filepath = $path;
        $media->metadata = json_encode([]);
        $media->is_private = $media_group['is_private'];
        $media->media_group_id = $media_group['id'];
        $media->mediable_id = $id;
        $media->mediable_type = $media_group['entity_type'];

        if ($media->save()) {
            return response()->json($media);
        }
        abort('404');
    }

    /**
     * Funtion to get the media
     *
     * @param Request $request  Request object
     * @param integer $media_id Media ID
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMediaAction(Request $request, int $media_id)
    {
        $media = Media::find($media_id);
        if (!is_object($media)) {
            abort(404, 'El archivo no existe', []);
        }
        return $media;
    }

    /**
     * Funcion que obtiene el archivo media privado
     *
     * @param Request $request  Request object
     * @param string  $media_id Media ID
     * @param string  $fname    Filename
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function proxyFile(Request $request, string $media_id, string $fname)
    {

        $media = Media::find($media_id);

        if (is_null($media)) {
            abort(404);
        }

        $filename = $media->filepath . $media->filename;

        $folder = 'private/';
        if (!boolval($media->is_private)) {
            abort(404);
        }

        $storage = Storage::disk('private');
        if (!$storage->exists($filename)) {
            abort(404);
        }

        $mimetype = $storage->mimeType($filename);
        $headers = ['Content-Type' => $mimetype];

        // TODO: Refactor adding permission access to private
        if (boolval($media->is_private) && !$storage->getVisibility($filename)) {
            // abort(404);
            $headers['Cache-Control'] = 'no-cache';
        }

        return new BinaryFileResponse(
            storage_path('app' . DIRECTORY_SEPARATOR . $folder . $filename),
            200,
            $headers
        );
    }

    /**
     * Funcion que permite validar la pertenencia de un archivo media con un usuario
     *
     * @param Request $request     Http request object
     * @param int     $id          Entity id.
     * @param string  $entity_name Entity name.
     * 
     * @return Json data
     */
    public function index(Request $request, int $id, string $entity_name)
    {
        
        $entity = '\\App\\Models\\' . Str::studly($entity_name);
        if (class_exists($entity)) {
            $instance = new $entity();
            $class_name = get_class($instance);

            $medias = Media::where(
                [
                'mediable_id' => $id,
                'mediable_type' => $class_name
                ]
            )->with(['mediaGroup'])->get();

            return response()->json($medias);
        }

        throw new PreconditionFailedHttpException;
    }

    /**
     * Retrieve nearest festa for the given latitude and longitude.
     *
     * @param Request $request Request object to be proccessed.
     * @param string  $id      entity id to be deleted.
     *
     * @return Response
     */
    public function delete(Request $request, string $id)
    {
        // TODO: verified entities couldn't be deleted.
        $media = Media::find($id);
        if (!is_null($media)) {
            $media->delete();
            return response()->json($media);
        }
        abort(404);
    }
}
