<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/GrapeTypeController.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GrapeType;
use Illuminate\Http\Request;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class GrapeTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
                'preferred_terrain' => 'bail|required|string|max:45',
                'preferred_climate' => 'bail|required|string|max:45',
            ]
        );

        $grape_type = new GrapeType($payload);

        $grape_type->save();

        return response()->json($grape_type);

    }


    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {
        $grape_types = GrapeType::get();

        return response()->json($grape_types);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $grape_type_id GrapeType ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $grape_type_id)
    {
        $grape_type = GrapeType::find($grape_type_id);

        if (!is_object($grape_type)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        return response()->json($grape_type);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $grape_type_id GrapeType ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $grape_type_id)
    {

        $grape_type = GrapeType::find($grape_type_id);

        if (!is_object($grape_type)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $grape_type->update($payload);

        return response()->json($grape_type);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request       Request object to be proccessed.
     * @param integer $grape_type_id GrapeType ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $grape_type_id)
    {
        $grape_type = GrapeType::find($grape_type_id);

        if (!is_object($grape_type)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        $grape_type->delete();

        return response()->json($grape_type);
    }

}
