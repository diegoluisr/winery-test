<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/AuthController.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessEmailJob;
use App\Models\Role;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function emailLogin(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'email' => 'bail|required|email|max:255',
                'password' => 'required|max:24',
            ]
        );

        $user = User::where('email', $payload['email'])->first();
        
        if (is_object($user)) {
            $user = $user->makeVisible(['password']);
        }
        
        if (!is_null($user)
            && Crypt::decrypt($user->password) == $payload['password']
        ) {
            return response()->json(
                $this->_createToken($user)
            );
        }

        abort(401, 'Email or password fail!');
    }

    /**
     * Funcion que registra un usuario usando email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON Response.
     */
    public function emailRegister(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'email' => 'bail|required|unique:users|email|max:255',
                'password' => 'required|min:8|max:24',
                'name' => 'bail|required|max:45',
            ]
        );

        $user = new User;
        $user->email = $payload['email'];
        $user->password = Crypt::encrypt($payload['password']);
        $user->name = $payload['name'];
        $user->role_id = Role::CUSTOMER;
        
        $user = $this->_createAccount($user);

        if ($user !== false) {
            return response()->json($user);
        }

        return response()->json(
            [
            'message' => 'Error creating your account!'
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Funcion que restablece una contraseña de usuario,
     * enviando al correo una temporal para acceder
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data
     */
    public function recoverPassword(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'email' => 'bail|required|email|max:255|exists:users,email'
            ]
        );
        
        $email = $payload['email'];
        $user = User::where('email', $email)->first();

        if (!empty($user)) {
            //se crea un password temporal
            $tmpPassword = Str::random(8);
            $user->password = Crypt::encrypt($tmpPassword);
            if ($user->save()) {
                // Se envia el correo con la informacion de la contraseña
                // temporal generada.
                // TODO: enviar email de contraseña temporal de recuperacion o
                // ponerlo en cola de envios
                dispatch(
                    (new ProcessEmailJob(
                        [
                            'subject' => 'Winery - password recovery',
                            'to' => [$email],
                            'template' => 'account.password.recovery',
                            'merge' => [
                                "code" => $tmpPassword
                            ],
                        ]
                    ))->onQueue('high')
                );
                
                return response()->json(
                    ["message" => "An email was sent to you"]
                );
            }
        }
        return response()->json(
            [
            'message' => 'Error recovering password!'
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Funcion que restablece una contraseña de usuario,
     * enviando al correo una temporal para acceder
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data
     */
    public function changePassword(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'password' => 'required|max:24',
            ]
        );

        $user = User::find($request->auth->id);

        if (!is_null($user)) {
            $user->password = Crypt::encrypt($payload['password']);
            $user->save();
            return response()->json($user);
        }

        abort('412');
    }

    /**
     * Funcion que actualiza un token de acceso de un usuario
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse token actualizado
     */
    public function tokenRenewal(Request $request)
    {
        return response()->json($this->_createToken($request->auth));
    }

    /**
     * Funcion que crea un usuario en la base de datos a partir de un objeto
     * User y Profile.
     * 
     * @param User    $user       User object.
     * @param boolean $send_email Set if the email must be sent.
     * 
     * @return User|FALSE retorna un usuario si se guardo o falso de lo contrario
     */
    private function _createAccount(User $user, bool $send_email = true)
    {
        if ($user->save()) {

            $user = User::find($user->id);

            dispatch(
                (new ProcessEmailJob(
                    [
                        'subject' => 'Winery - New account',
                        'to' => [$user->email],
                        'template' => 'account.create',
                        'merge' => [
                            "name" => $user->name
                        ],
                    ]
                ))->onQueue('high')
            );
            
            return $user;
        }
        return false;
    }

    /**
     * Funcion que crea un token para un usuario.
     *
     * @param User $user usuario Objeto user.
     * 
     * @return array arreglo con la informacion del token de acceso.
     */
    private function _createToken(User $user)
    {
        $data = [
            'iat' => time(),
            'nbf' => time(),
            'sub' => $user->id,
            'exp' => time() + (60 * 60 * 24 * 7) // Seven days expiration time
        ];

        $jwt = JWT::encode($data, env('JWT_SECRET'));

        return [
            'type' => 'Bearer',
            'token' => $jwt
        ];
    }

}
