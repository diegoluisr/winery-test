<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/WineryController.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Winery;
use Illuminate\Http\Request;


/**
 * Clase para el manejo en la subida de archivos a la plataforma
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class WineryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
                'founding_year' => 'required|bail|string|min:3|max:4',
                'region' => 'bail|required|string|max:45',
            ]
        );

        $winery = new Winery($payload);

        $winery->save();

        return response()->json($winery);

    }


    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {
        $wineries = Winery::with(['cover'])->get();

        return response()->json($wineries);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request   Request object to be proccessed.
     * @param integer $winery_id Winery ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $winery_id)
    {
        $winery = Winery::with(['cover'])->find($winery_id);

        if (!is_object($winery)) {
            abort(404, 'The winery doesn\'t exists');
        }

        return response()->json($winery);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request   Request object to be proccessed.
     * @param integer $winery_id Winery ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $winery_id)
    {

        $winery = Winery::find($winery_id);

        if (!is_object($winery)) {
            abort(404, 'The winery doesn\'t exists');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
                'founding_year' => 'required|bail|string|min:3|max:4',
                'region' => 'bail|required|string|max:45',
            ]
        );

        $winery->update($payload);

        return response()->json($winery);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request   Request object to be proccessed.
     * @param integer $winery_id Winery ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $winery_id)
    {
        $winery = Winery::find($winery_id);

        if (!is_object($winery)) {
            abort(404, 'The winery doesn\'t exists');
        }

        $winery->delete();

        return response()->json($winery);
    }

}
