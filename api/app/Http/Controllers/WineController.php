<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/WineController.php
 * 
 * PHP version 7
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Wine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

/**
 * Clase para el manejo en la subida de archivos a la plataforma
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class WineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
                'unit_id' => 'bail|required|integer|exists:units,id',
                'winery_id' => 'bail|required|integer|exists:wineries,id',
                'grape_type_id' => 'bail|required|integer|exists:grape_types,id',
                'wine_type_id' => 'bail|required|integer|exists:wine_types,id',
                'classification_id' => 'nullable|integer|exists:classifications,id',
                'quantity' => 'bail|required|integer|min:1',
                'name' => 'bail|required|string|min:1|max:45',
                'year' => 'bail|required|date_format:Y-m',
                'age_until' => 'bail|required|integer|min:1',
                'description' => 'bail|required|string|max:45',
                'value' => 'bail|required|numeric|min:0.0',
                'taste_notes' => 'nullable|array',
                'taste_notes.*.taste_note_id'
                    => 'bail|required|integer|exists:taste_notes,id',
                'food_groups' => 'nullable|array',
                'food_groups.*.food_group_id'
                    => 'bail|required|integer|exists:food_groups,id',
            ]
        );

        $payload['user_id'] = $request->user()->id;

        $wine = new Wine($payload);

        if (Gate::denies('create-wine', $wine)) {
            abort(403, 'User can\'t create wine');
        }

        $wine->save();

        if (isset($payload['taste_notes'])) {
            $taste_notes_ids = [];
            foreach ($payload['taste_notes'] as $taste_note) {
                $taste_notes_ids[] = $taste_note['taste_note_id'];
            }
            $wine->tasteNotes()->attach($taste_notes_ids);
        }

        if (isset($payload['food_groups'])) {
            $food_groups_ids = [];
            foreach ($payload['food_groups'] as $food_group) {
                $food_groups_ids[] = $food_group['food_group_id'];
            }
            $wine->foodGroups()->attach($food_groups_ids);
        }

        return response()->json($wine);

    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {

        // Getting query string params
        $payload = $this->validate(
            $request, [
                'filter' => 'nullable|string|'
                    . 'in:classification,wine_type,grape_type,unit,winery',
                'id' => 'required_if:filter,'
                    . 'classification,wine_type,grape_type,unit,winery|integer',
                'year' => 'nullable|integer|min:0',
                'best_drinkable' => 'nullable|integer|min:0',
                'limit' => 'nullable|numeric|min:1',
                'q' => 'nullable|string|min:3'
            ]
        );

        // Setting a default limit it doesn't exist
        if (!isset($payload['limit'])) {
            $payload['limit'] = 20;
        }

        // Getting the words to prepare the search
        $words = [];
        if (isset($payload['q'])) {
            $q = $payload['q'];
            $q = str_replace(['_', '>', '<'], ' ', $q);
            $words = explode(' ', $q);
        }

        // Getting the filter param value
        $filter = $payload['filter'] ?? null;

        // Build the query
        $query = Wine::with(
            [
                'cover',
                'tasteNotes',
                'foodGroups',
                'classification',
                'unit',
                'grapeType',
                'wineType',
                'user',
            ]
        );

        // Adding conditions based on words set on query
        if (count($words) > 0) {
            $query->where(
                function ($queryOr) use ($words) {
                    foreach ($words as $word) {
                        if (strlen($word) >= 3) {
                            $queryOr->orWhere('name', 'LIKE', '%' . $word . '%');
                        }
                    }
                }
            );
        }

        // Adding filter to the search if <> null
        if (!is_null($filter)
            && in_array(
                $filter,
                ['classification', 'wine_type', 'grape_type', 'unit', 'winery']
            )
        ) {
            $query->where($filter . '_id', $payload['id']);
        }

        // Adding year filter
        if (isset($payload['year'])) {
            $query->whereYear('year', $payload['year']);
        }
        
        // Adding best drinkable filter
        if (isset($payload['best_drinkable'])) {
            $query->whereRaw(
                'YEAR(DATE_ADD(year, INTERVAL age_until YEAR)) = ?',
                [$payload['best_drinkable']]
            );
        }

        // Wines pagination using limit
        $wines = $query->paginate($payload['limit']);

        return response()->json($wines);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * @param integer $wine_id Wine ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $wine_id)
    {
        $wine = Wine::with(
            [
                'cover',
                'tasteNotes',
                'foodGroups',
                'classification',
                'unit',
                'grapeType',
                'wineType',
            ]
        )
            ->find($wine_id);

        if (!is_object($wine)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        return response()->json($wine);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * @param integer $wine_id Wine ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $wine_id)
    {
        $wine = Wine::find($wine_id);

        if (!is_object($wine)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        if (Gate::denies('update-wine', $wine)) {
            abort(403, 'User can\'t update a wine record');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
                'unit_id' => 'bail|required|integer|exists:units,id',
                'winery_id' => 'bail|required|integer|exists:wineries,id',
                'grape_type_id' => 'bail|required|integer|exists:grape_types,id',
                'wine_type_id' => 'bail|required|integer|exists:wine_types,id',
                'classification_id' => 'nullable|integer|exists:classifications,id',
                'quantity' => 'bail|required|integer|min:1',
                'name' => 'bail|required|string|min:1|max:45',
                'year' => 'bail|required|string|min:4',
                'age_until' => 'bail|required|integer|min:1',
                'description' => 'bail|required|string|max:45',
                'value' => 'bail|required|numeric|min:0.0',
                'taste_notes' => 'nullable|array',
                'taste_notes.*.taste_note_id'
                    => 'bail|required|integer|exists:taste_notes,id',
                'food_groups' => 'nullable|array',
                'food_groups.*.food_group_id'
                    => 'bail|required|integer|exists:food_groups,id',
            ]
        );

        $wine->update($payload);

        if (isset($payload['taste_notes'])) {
            $taste_notes_ids = [];
            foreach ($payload['taste_notes'] as $taste_note) {
                $taste_notes_ids[] = $taste_note['taste_note_id'];
            }
            $wine->tasteNotes()->sync($taste_notes_ids);
        }

        if (isset($payload['food_groups'])) {
            $food_groups_ids = [];
            foreach ($payload['food_groups'] as $food_group) {
                $food_groups_ids[] = $food_group['food_group_id'];
            }
            $wine->foodGroups()->sync($food_groups_ids);
        }

        return response()->json($wine);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * @param integer $wine_id Wine ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $wine_id)
    {

        $wine = Wine::find($wine_id);
        
        if (!is_object($wine)) {
            abort(404, 'The grape type doesn\'t exists');
        }
        
        if (Gate::denies('create-wine', $wine)) {
            abort(403, 'User can\'t delete a wine record');
        }

        $wine->delete();

        return response()->json($wine);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * @param integer $wine_id Wine ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function removeOpenedBottle(Request $request, int $wine_id)
    {
        $wine = Wine::find($wine_id);

        if (!is_object($wine)) {
            abort(404, 'The grape type doesn\'t exists');
        }

        if (Gate::denies('update-wine', $wine)) {
            abort(403, 'User can\'t update a wine record');
        }

        if ($wine->quantity > 0) {
            $wine->quantity = $wine->quantity - 1;
        }

        $wine->update();

        return response()->json($wine);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function timeline(Request $request)
    {
        $timeline = [];

        $all_wines = Wine::select(
            'wines.*',
            DB::raw('DATE_ADD(year, INTERVAL age_until YEAR) best_drinkable_after')
        )
            ->orderby('best_drinkable_after', 'asc')
            ->get();

        foreach ($all_wines as $wine) {
            $parts = explode('-', $wine->best_drinkable_after);

            $year = $parts[0];
            $month = $parts[1];

            $timeline[$year][$month][] = $wine;
        } 

        return response()->json($timeline);
    }

}
