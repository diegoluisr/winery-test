<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/WineTypeController.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WineType;
use Illuminate\Http\Request;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class WineTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $wine_type = new WineType($payload);

        $wine_type->save();

        return response()->json($wine_type);

    }


    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {
        $wine_types = WineType::get();

        return response()->json($wine_types);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request      Request object to be proccessed.
     * @param integer $wine_type_id WineType ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $wine_type_id)
    {
        $wine_type = WineType::find($wine_type_id);

        if (!is_object($wine_type)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        return response()->json($wine_type);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request      Request object to be proccessed.
     * @param integer $wine_type_id Wine type ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $wine_type_id)
    {

        $wine_type = WineType::find($wine_type_id);

        if (!is_object($wine_type)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $wine_type->update($payload);

        return response()->json($wine_type);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request      Request object to be proccessed.
     * @param integer $wine_type_id WineType ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $wine_type_id)
    {
        $wine_type = WineType::find($wine_type_id);

        if (!is_object($wine_type)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        $wine_type->delete();

        return response()->json($wine_type);
    }

}
