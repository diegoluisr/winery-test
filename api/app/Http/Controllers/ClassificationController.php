<?php
/**
 * This document is open source
 * file: api/app/Http/Controllers/ClassificationController.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Classification;
use Illuminate\Http\Request;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class ClassificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function create(Request $request)
    {
        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $classification = new Classification($payload);

        $classification->save();

        return response()->json($classification);

    }


    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request Request object to be proccessed.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getList(Request $request)
    {
        $classifications = Classification::get();

        return response()->json($classifications);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request           Request object to be proccessed.
     * @param integer $classification_id Classification ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function getOne(Request $request, int $classification_id)
    {
        $classification = Classification::find($classification_id);

        if (!is_object($classification)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        return response()->json($classification);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request           Request object to be proccessed.
     * @param integer $classification_id Wine type ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function update(Request $request, int $classification_id)
    {

        $classification = Classification::find($classification_id);

        if (!is_object($classification)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        $payload = $this->validate(
            $request, [
                'name' => 'bail|required|string|max:45',
            ]
        );

        $classification->update($payload);

        return response()->json($classification);
    }

    /**
     * Funcion que loguea un usuario por medio de un email y contraseña.
     *
     * @param Request $request           Request object to be proccessed.
     * @param integer $classification_id Classification ID.
     * 
     * @return \Illuminate\Http\JsonResponse data JSON response.
     */
    public function delete(Request $request, int $classification_id)
    {
        $classification = Classification::find($classification_id);

        if (!is_object($classification)) {
            abort(404, 'The wine type doesn\'t exists');
        }

        $classification->delete();

        return response()->json($classification);
    }

}
