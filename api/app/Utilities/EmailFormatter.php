<?php
/**
 * This document is open source
 * file: api/app/Utilities/EmailFormatter.php
 * 
 * PHP version 7
 * 
 * @category Utility
 * @package  App\Utilities
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Utilities;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Utility
 * @package  App\Utilities
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class EmailFormatter
{
    /**
     * Function to prepare data array for "Elastic Email" provider
     * 
     * @param array $extend Base array to be extended
     * 
     * @return array
     */
    public static function prepareForElastic(array $extend)
    {
        $data = [
            'subject' => null, 
            'from' => env('EMAIL_DEFAULT_FROM'), 
            'fromName' => env('EMAIL_DEFAULT_FROM_NAME'), 
            'sender' => null, 
            'senderName' => null, 
            'msgFrom' => null, 
            'msgFromName' => null, 
            'replyTo' => env('EMAIL_DEFAULT_REPLY_TO'), 
            'replyToName' => env('EMAIL_DEFAULT_REPLY_TO_NAME'), 
            'to' => [],
            'msgTo' => [],
            'msgCC' => [],
            'msgBcc' => [],
            'lists' => [],
            'segments' => [], 
            'mergeSourceFilename' => null, 
            'dataSource' => null, 
            'channel' => null, 
            'bodyHtml' => null, 
            'bodyText' => null, 
            'charset' => env('EMAIL_DEFAULT_CHARSET'), 
            'charsetBodyHtml' => null, 
            'charsetBodyText' => null, 
            'encodingType' => \ElasticEmailEnums\EncodingType::None, 
            'template' => null,
            'attachmentFiles' => [],
            'headers' => [], 
            'postBack' => null,
            'merge' => [], 
            'timeOffSetMinutes' => null, 
            'poolName' => null, 
            'isTransactional' => true,
            'attachments' => [], 
            'trackOpens' => null, 
            'trackClicks' => null
        ];

        foreach ($data as $key => $value) {
            if (isset($extend[$key]) && !empty($extend[$key])) {
                $data[$key] = $extend[$key];
            }
        }

        return $data;
    }
}