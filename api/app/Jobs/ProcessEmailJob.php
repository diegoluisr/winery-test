<?php
/**
 * This document is open source
 * file: api/app/Jobs/ProcessEmailJob.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Jobs
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Jobs;

/**
 * Clase para gestionar los trabajos en el envio de emails.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class ProcessEmailJob extends Job
{

    protected $data;

    /**
     * Create a new job instance.
     * 
     * @param array $data Arreglo con los datos necesarios para el Job.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailClient = app(\ElasticEmailClient\ElasticClient::class);
        call_user_func_array(
            [$emailClient->Email, 'Send'],
            \App\Utilities\EmailFormatter::prepareForElastic($this->data)
        );
    }
}