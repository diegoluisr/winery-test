<?php
/**
 * This document is open source
 * file: api/app/Events/Event.php
 * 
 * PHP version 7
 * 
 * @category Job
 * @package  App\Events
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Abstract class for event.
 * 
 * @category Events
 * @package  App\Events
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
abstract class Event
{
    use SerializesModels;
}
