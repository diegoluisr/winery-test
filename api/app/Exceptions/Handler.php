<?php
/**
 * This document is open source
 * file: api/app/Exceptions/Handler.php
 * 
 * PHP version 7
 * 
 * @category Exceptions
 * @package  App\Exceptions
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Dotenv\Exception\ValidationException as DotentValidationException;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;
/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category Exceptions
 * @package  App\Exceptions
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  https://en.wikipedia.org/wiki/MIT_License MIT
 * @link     https://bitbucket.org/diegoluisr/winery-test/src/master/LICENCE.md
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception Exception
     * 
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request   Http request object
     * @param \Exception               $exception Exception
     * 
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if (env('APP_DEBUG')) {
            return parent::render($request, $exception);
        }

        // https://gist.github.com/joseluisq/bea6220cca5e1441d550b27409283497
        $class = get_class($exception);
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        $message = '';
        $data = $request->all();
        if ($exception instanceof HttpResponseException) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        } elseif ($exception instanceof MethodNotAllowedHttpException) {
            $status = Response::HTTP_METHOD_NOT_ALLOWED;
            $exception = new MethodNotAllowedHttpException(
                [],
                'HTTP_METHOD_NOT_ALLOWED',
                $exception
            );
        } elseif ($exception instanceof NotFoundHttpException) {
            $status = Response::HTTP_NOT_FOUND;
            $exception = new NotFoundHttpException('HTTP_NOT_FOUND', $exception);
        } elseif ($exception instanceof AuthorizationException) {
            $status = Response::HTTP_FORBIDDEN;
            $exception = new AuthorizationException('HTTP_FORBIDDEN', $status);
        } elseif ($exception instanceof ValidationException
            && $exception->getResponse()
        ) {
            $status = Response::HTTP_BAD_REQUEST;
            $exception = new DotenvValidationException(
                'HTTP_BAD_REQUEST',
                $status,
                $exception
            );
        } elseif ($exception instanceof IlluminateValidationException
            && $exception->getResponse()
        ) {
            $status = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = $exception->getResponse()->getOriginalContent();
        } elseif ($exception instanceof SymfonyHttpException) {
            $data = $exception->getTraceAsString();
            $message = $exception->getMessage();
        } elseif ($exception instanceof \InvalidArgumentException) {
            $data = $exception->getTraceAsString();
            $message = $exception->getMessage();
        } else {
            $message = $exception->getMessage();
        }

        return response()->json(
            [
                'class' => $class,
                'message' => $message,
                'data' => $data,
                'status' => $status,
            ], $status
        );
    }
}
